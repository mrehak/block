#!/usr/local/bin/perl

=pod

=head1 NAME

B<block> - Manage Mikrotik address list and scan suricata eve files

=head1 SYNOPSIS

block [global options] command <arguments>

=head1 OPTIONS

=over

=item block command [--verbose|--debug] <arguments>

Prints this message.

=item --verbose

Prints the messages going to syslog to stderr also.

=item --debug

Prints debugging info. 

=back

=cut

use strict;
use warnings;

use Data::Dumper;
use English;
use Fcntl qw(SEEK_END);
use File::Basename;
use Getopt::Long;
use IO::Handle;
use JSON qw(decode_json);
use MikroTik::API;
use Pod::Usage;
use Socket;
use Sys::Syslog qw(:standard :macros);

my %args;

my $alerts = 0;
my $alerts_adds = 0;

my $EVE_FILE = "/var/log/suricata/eve.json";
my $MY_NETWORK_RE = "192\.168\.|213\.168\.|81\.25\.19\.132";
my $TIMEOUT = '7d';
my $TAIL_NAPTIME = 1;

#
# Internal
#

sub process {
	my $api = shift;
	my $entry = shift;
	my $msg = "";

	chomp $entry;
	my $newline = index($entry, "\n");
	my $string = $newline == -1? $entry: substr($entry, 0, $newline);
	die "multiple lines in input" if ($newline != -1);

	my $json = decode_json($string);
	print "decoded json: " . Dumper($json) if $args{debug};

	$msg .= "Missing timestamp. " unless(defined $json->{timestamp});
	$msg .= "Missing event_type. " unless(defined $json->{event_type});
	$msg .= "Missing dest_ip. " unless(defined $json->{dest_ip});
	$msg .= "Missing src_ip. " unless(defined $json->{src_ip});
	$msg .= "Missing alert->gid. " unless(defined $json->{alert}->{gid});
	$msg .= "Missing alert->signature_id. " unless(defined $json->{alert}->{signature_id});
	$msg .= "Missing alert->rev. " unless(defined $json->{alert}->{rev});
	$msg .= "Missing alert->category. " unless(defined $json->{alert}->{category});
	$msg .= "Missing alert->signature. " unless(defined $json->{alert}->{signature});

	warn "$json->{timestamp}: $msg" if length($msg);

	my $addr;
	$addr = $json->{dest_ip} unless $json->{dest_ip} =~ $MY_NETWORK_RE;
	$addr = $json->{src_ip} unless $json->{src_ip} =~ $MY_NETWORK_RE;
	# ignore internal network communication
	return unless defined $addr;

	$alerts++;
	return 3 if check_address($api, $addr);

	# try to resolve the address
	my $iaddr = inet_aton($addr);
	my $host = gethostbyaddr($iaddr, AF_INET);
	
	return add_address($api,
		$addr,
		$json->{alert}->{gid},
		$json->{alert}->{signature_id},
		$json->{alert}->{rev},
		$host,
		$json->{alert}->{category},
		$json->{alert}->{signature});
}

sub get_api {
	return MikroTik::API->new({
		host => 'gw.tekkirk.org',
		username => 'block',
		password => 'bI#6d2NN3n4h',
		use_ssl => 1,
				  });
}

sub get_blocked {
	my $api = shift;

	my ($ret, @list) = $api->query('/ip/firewall/address-list/print', {}, {'list' => 'block'});

	return \@list;
}

sub list_blocked {
	my $api = shift;

	my $list = get_blocked($api);

	foreach my $entry (@$list) {
		print "$entry->{address}\t$entry->{timeout}\t$entry->{comment}\n";
	}
	print scalar @$list . " entries\n" if $args{verbose};
}

sub add_address {
	my $api = shift;
	my $address = shift;
	my $gid = shift;
	my $signature_id = shift;
	my $rev = shift;
	my $host = shift;
	my $category = shift;
	my $signature = shift;

	my $comment;
	$comment  = "[$gid:$signature_id:$rev]" if defined $gid and defined $signature_id and defined $rev;
	$comment .= " $signature [$category]" if defined $category and defined $signature;
	$comment .= " ($host)" if defined $host;

	my $prop = {'list' => 'block',
			'address' => $address,
			'timeout' => $TIMEOUT,
			'comment' => $comment};

	$alerts_adds++;

	my $message = "$alerts_adds/$alerts $address $comment";
	print "$message\n" if $args{verbose};
	syslog(LOG_INFO|LOG_LOCAL7, $message);

	return $api->cmd('/ip/firewall/address-list/add', $prop);
}

sub remove_address {
	my $api = shift;
	my $address = shift;

	my $id = check_address($api, $address);
	return 1 unless $id;

	return $api->cmd('/ip/firewall/address-list/remove', { 'numbers' => $id } );
}

sub remove_all_addresses {
	my $api = shift;
	my $list = get_blocked($api);

	foreach my $entry (@$list) {
		remove_address($api, $entry->{address});
	}

	return 0;
}

sub check_address {
	my $api = shift;
	my $address = shift;

	my ($ret, @list) = $api->query('/ip/firewall/address-list/print', {},
				       {'list' => 'block', 'address' => $address});

	return $list[0]->{'.id'} if scalar @list;
	return undef;
}

#
# Commands
#

my %commands = (
	watch  => \&watch,
	list   => \&list,
	add    => \&add,
	remove => \&remove,
	help   => 'help',
);

=pod

=over

=item watch [--whole] <file>

Watch the file and update Mikrotik address list for each line read. File
defaults to /var/log/suricata/eve.json if it is not specified.

=over

=item --whole

Process the whole file and stop at the end.

=back

=back

=cut

sub watch {
	my $fh;
	my $line;

	GetOptions(\%args, "verbose", "debug", "whole") or exit 2;

	my $file = shift @ARGV;
	$file = $EVE_FILE unless defined $file;

	my $api = get_api;

	my $inode = (stat($file))[1];
	open($fh, "<", $file)
	    or die "Can't open $file: $!";

	seek($fh, 0, SEEK_END) unless defined $args{whole};

	for (;;) {
		while ($line = <$fh>) {
			process($api, $line);
		}

		# check for rename/rotate
		my $new_inode = (stat($file))[1];
		if ($new_inode != $inode) {
			sleep $TAIL_NAPTIME; # prevent race
			$inode = $new_inode;
			close($fh);
			open($fh, "<", $file)
			    or die "Can't open $file: $!";
			print "Watched file rotated, re-opened.\n" if $args{verbose};
		}

		sleep $TAIL_NAPTIME;
		$fh->clearerr();
	}

	close($fh);

	$api->logout();

	return 0;
}

=pod

=over

=item list

List addresses on the list.

=back

=cut

sub list {
	GetOptions(\%args, "verbose", "debug") or exit 2;

	my $api = get_api;
	list_blocked($api);
	$api->logout();

	return 0;
}

=pod

=over

=item add <address>

Add address to the list.

=back

=cut

sub add {
	my $address = shift @ARGV;
	return 1 unless defined $address;

	GetOptions(\%args, "verbose", "debug") or exit 2;

	my $api = get_api;
	return 3 if check_address($api, $address);
	my $ret = add_address($api, $address);
	$api->logout();

	return $ret;
}

=pod

=over

=item remove [-all] <address>

Remove address from the list.

=over

=item --all

Remove all addresses on the block list.

=back

=back

=cut

sub remove {
	my $address = shift @ARGV;
	return 1 unless defined $address xor defined $args{all};

	GetOptions(\%args, "verbose", "debug", "all") or exit 2;

	my $api = get_api;
	remove_address($api, $address) if defined $address;
	remove_all_addresses($api) if defined $args{all};
	$api->logout();

	return 0;
}

=pod

=head1 COMMANDS

=over

=item help

Writes help.

=back

=cut

sub help {
	pod2usage('-exitval' => 2, '-verbose' => 2);
}

#
# Main
#

help unless (@ARGV);

my $command = shift @ARGV;
my @cmnds = grep m/^\Q$command\E/, keys %commands;
$command = $cmnds[0] if (@cmnds);

help if ($command =~ m/^(help|-h|--help)$/);
help unless (@cmnds);

openlog(basename($PROGRAM_NAME), '', LOG_LOCAL7);

exit &{$commands{$command}};

=head1 DESCRIPTION

Block manages Mikrotik address list and adds new entries from Suricata
EVE files to the address list.

=cut

__END__
