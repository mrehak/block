The block.pl script can watch suricata output eve.json file and update the IP address list on Mikrotik router. It is designed as a trivial IDS system.

You have to configure suricata system first. Than, you have to configure Mikrotik router to block IPs on the list. It is written basically for FreeBSD system but it could be easilly adoped to any other UNIX system.
